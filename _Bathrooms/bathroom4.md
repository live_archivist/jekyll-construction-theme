---
layout: project
name: Bathroom 4
intro: "Bathroom remodel where we did custom vanities."
carousel: true
carousel_items:
  - {"path": "/assets/collections/bathrooms/bathroom4/1.jpg","title": "Bathroom 4-1","desc": "Bathroom.", "position": "0"} # First item always position 0
  - {"path": "/assets/collections/bathrooms/bathroom4/2.jpg","title": "Bathroom 4-2","desc": "Bathroom.", "position": "1"}
---