---
layout: project
name: Bathroom 7
intro: "Bathroom remodel where we did custom vanities."
carousel: true
carousel_items:
  - {"path": "/assets/collections/bathrooms/bathroom7/1.jpg","title": "Bathroom 7-1","desc": "Bathroom.", "position": "0"} # First item always position 0
  - {"path": "/assets/collections/bathrooms/bathroom1/2.jpg","title": "Bathroom 7-2","desc": "Bathroom.", "position": "1"}
---