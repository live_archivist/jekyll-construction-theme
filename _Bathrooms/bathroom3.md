---
layout: project
name: Bathroom 3
intro: "Bathroom remodel where we did custom vanities."
carousel: true
carousel_items:
  - {"path": "/assets/collections/bathrooms/bathroom3/1.jpg","title": "Bathroom 3-1","desc": "Bathroom.", "position": "0"} # First item always position 0
  - {"path": "/assets/collections/bathrooms/bathroom3/2.jpg","title": "Bathroom 3-2","desc": "Bathroom.", "position": "1"}
---