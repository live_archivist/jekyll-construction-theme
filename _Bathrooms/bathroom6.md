---
layout: project
name: Bathroom 6
intro: "Bathroom remodel where we did custom vanities."
carousel: true
carousel_items:
  - {"path": "/assets/collections/bathrooms/bathroom6/1.jpg","title": "Bathroom 6-1","desc": "Bathroom.", "position": "0"} # First item always position 0
  - {"path": "/assets/collections/bathrooms/bathroom6/2.jpg","title": "Bathroom 6-2","desc": "Bathroom.", "position": "1"}
---