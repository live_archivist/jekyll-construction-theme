---
layout: project
name: Bathroom 2
intro: "Bathroom remodel where we did custom vanities."
carousel: true
carousel_items:
  - {"path": "/assets/collections/bathrooms/bathroom2/1.jpg","title": "Bathroom 1-1","desc": "Bathroom.", "position": "0"} # First item always position 0
  - {"path": "/assets/collections/bathrooms/bathroom2/2.jpg","title": "Bathroom 1-2","desc": "Bathroom.", "position": "1"}
---