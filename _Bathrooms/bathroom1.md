---
layout: project
name: Bathroom 1
intro: "Bathroom remodel where we did custom vanities."
carousel: true
carousel_items:
  - {"path": "/assets/collections/bathrooms/bathroom1/1.jpg","title": "Bathroom 1-1","desc": "Bathroom.", "position": "0"} # First item always position 0
  - {"path": "/assets/collections/bathrooms/bathroom1/2.jpg","title": "Bathroom 1-2","desc": "Bathroom.", "position": "1"}
---