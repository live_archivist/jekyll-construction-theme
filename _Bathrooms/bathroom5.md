---
layout: project
name: Bathroom 5
intro: "Bathroom remodel where we did custom vanities."
carousel: true
carousel_items:
  - {"path": "/assets/collections/bathrooms/bathroom5/1.jpg","title": "Bathroom 5-1","desc": "Bathroom.", "position": "0"} # First item always position 0
  - {"path": "/assets/collections/bathrooms/bathroom5/2.jpg","title": "Bathroom 5-2","desc": "Bathroom.", "position": "1"}
---