---
layout: project
name: Office 1
intro: "Custom Office cabinets."
carousel: true
carousel_items:
  - {"path": "/assets/collections/other/office1/1.jpg","title": "Office 1-1","desc": "Office.", "position": "0"} # First item always position 0
  - {"path": "/assets/collections/other/office1/2.jpg","title": "Office 1-2","desc": "Office.", "position": "1"}
---