---
layout: project
name: Mudroom 1
intro: "Custom mudroom cabinets."
carousel: true
carousel_items:
  - {"path": "/assets/collections/other/mudroom1/1.jpg","title": "Mudroom 1-1","desc": "Mudroom.", "position": "0"} # First item always position 0
  - {"path": "/assets/collections/other/mudroom1/2.jpg","title": "Mudroom 1-2","desc": "Mudroom.", "position": "1"}
---