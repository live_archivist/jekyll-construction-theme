---
layout: project
name: Kitchen 6
intro: "Full kitchen remodel with custom cabinets."
carousel: true
carousel_items:
  - {"path": "/assets/collections/kitchens/kitchen6/1.jpg","title": "Kitchen 6-1","desc": "Kitchen.", "position": "0"} # First item always position 0
  - {"path": "/assets/collections/kitchens/kitchen6/2.jpg","title": "Kitchen 6-2","desc": "Kitchen.", "position": "1"}
---