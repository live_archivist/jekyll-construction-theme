---
layout: project
name: Kitchen 4
intro: "Full kitchen remodel with custom cabinets."
carousel: true
carousel_items:
  - {"path": "/assets/collections/kitchens/kitchen4/1.jpg","title": "Kitchen 4-1","desc": "Kitchen.", "position": "0"} # First item always position 0
  - {"path": "/assets/collections/kitchens/kitchen4/2.jpg","title": "Kitchen 4-2","desc": "Kitchen.", "position": "1"}
---