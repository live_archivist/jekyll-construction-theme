---
layout: project
name: Kitchen 3
intro: "Full kitchen remodel with custom cabinets."
carousel: true
carousel_items:
  - {"path": "/assets/collections/kitchens/kitchen3/1.jpg","title": "Kitchen 3-1","desc": "Kitchen.", "position": "0"} # First item always position 0
  - {"path": "/assets/collections/kitchens/kitchen3/2.jpg","title": "Kitchen 3-2","desc": "Kitchen.", "position": "1"}
---