---
layout: project
name: Kitchen 5
intro: "Full kitchen remodel with custom cabinets."
carousel: true
carousel_items:
  - {"path": "/assets/collections/kitchens/kitchen5/1.jpg","title": "Kitchen 5-1","desc": "Kitchen.", "position": "0"} # First item always position 0
  - {"path": "/assets/collections/kitchens/kitchen5/2.jpg","title": "Kitchen 5-2","desc": "Kitchen.", "position": "1"}
---