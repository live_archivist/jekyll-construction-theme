---
layout: project
name: Kitchen 1
intro: "Full kitchen remodel with custom cabinets."
carousel: true
carousel_items:
  - {"path": "/assets/collections/kitchens/kitchen1/1.jpg","title": "Kitchen 1-1","desc": "Kitchen.", "position": "0"} # First item always position 0
  - {"path": "/assets/collections/kitchens/kitchen1/2.jpg","title": "Kitchen 1-2","desc": "Kitchen.", "position": "1"}
---